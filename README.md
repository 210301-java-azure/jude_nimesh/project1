`Project-1: Music Store.`

This project is an extension of project-0 where the back-end was deployed and project1 will extend the project by implementing the front-end to interact with the back-end. The objective of the project is to implemente REST API with RESTfull form to signup and login, and access to the database's listed items.

`Music store application will allow:`
- List all items in the stock based on musical department. This uses the GET method to assess the stock list.
- Able to add an item to the product list using the POST method
- Able to update an item in the product list using the PUT method
- Able to delete an item from the product list using the DELETE method

Similarly as above, the CRUD method is also used to place, update, delete, and list an order by id or list all orders.

The changes in the front end reflect on the Azure cloud sequal database.

Dues to time constraints, the following applications were not implemented: 
  - Password encription and security
  - Roles for Admin, Employee and Customer(default)
  - Thrid party website interaction

`Technology used in this project:`

`back-end:`
   - Hibernate  
   - JDBC
   - SQL database (Azure cloud DB)
   - DBeaver( access the database and monitor able to make changes in database)
   - Jenkins (Azure cloud Virtual Machin(VM))
   - Docker
   - Gradle
   - Postman

 `Front-end:`
   - HTML
   - CCS
   - Java Script
   - AJAX ( & Fetch API)
   - Http calls via AJAX
   - Blob storage(Azure blob storage)

`Platform:`
   - Azure cloud storage   

Usage:
This project - Music Store allows a customer to create an account and login. 
This will give access for a customer to view all available product items and create, update, and delete orders.

Conclusion:
This is the first front-end project I worked on and the objective is to retrieve information from the data base and make sure all CRUD methods works. This project presented during the class review and program worked perfectly.

Parameters marked with an askterisk (*) are required.  
`signup`  
|Method|Role|Form Parameter|Result|
|---|---|---|---|
|Post|None|username*, password*, email*|Registers a new user account with the supplied credentials|
  

`login`  
|Method|Role|Form Parameters|Result|
|---|---|---|---|
|Get|None||If a valid auth-token is found in the header, alerts user of login and privilege level|
|Post|Any|username*, password*|Attempts to log into the associated user account and if successful, returns an auth-token|   

## Login Page  
![IMAGE](./docs/01-Login-page.png)


## Home Page 
![IMAGE](./docs/02-Home-Page.png)

## Customer Page 
![IMAGE](./docs/03-Customer-page-List.png)

## Product Page 
![IMAGE](./docs/04-Product-List-page.png)

## Order Page 
![IMAGE](./docs/05-Order-page.png)

## Customer list Page 
![IMAGE](./docs/06-Customer-registered-list.png)

## Adding New Customer Page 
![IMAGE](./docs/07-Add-New-Customer.png)

## Update Customer Page 
![IMAGE](./docs/08A-Update-Customer-Record.png)
## Update Customer Page cont ...
![IMAGE](./docs/08B-Update-Customer-Record-Select.png)

## Delete Customer Page
![IMAGE](./docs/09-Delete-Customer-record.png)

## Product Items available list Page 
![IMAGE](./docs/10-Product-Inventory-List.png)

Note: more web page screen shots are available via docs directory
