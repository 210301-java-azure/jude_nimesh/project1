document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event){
    event.preventDefault(); // this stops the form from sending a GET request by default to the same URL
    const username = document.getElementById("inputEmail").value;
    const password = document.getElementById("inputPassword").value;
    //console.log(username, password);
    ajaxLogin(username, password, successfulLogin, loginFailed)
}

function successfulLogin(xhr){
    console.log("yay success");
    const authToken = xhr.getResponseHeader("Authorization");
    sessionStorage.setItem("Authorization", authToken);
    window.location.href = "./home-main.html";
}

function loginFailed(xhr){
    // console.log("oh no something went wrong");
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
    errorDiv.innerText = xhr.responseText;
}

