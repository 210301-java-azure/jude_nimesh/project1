const token = sessionStorage.getItem("Authorization");

// take this token, and check it against your server to make sure it is a valid token
console.log(token);

if(token){
    // if there is a token, we allow users to create a new item and to log out, if not we allow them to log in
    document.getElementById("login-nav-item").hidden = true;
    document.getElementById("new-item-nav-item").hidden = false;
    document.getElementById("log-out-nav-item").hidden = false;
}

// associate the logout functionality with clicking on the logout item on the nav bar
document.getElementById("log-out-nav-item").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("Authorization");
    document.getElementById("login-nav-item").hidden = false;
    document.getElementById("new-item-nav-item").hidden = true;
    document.getElementById("log-out-nav-item").hidden = true;
}
