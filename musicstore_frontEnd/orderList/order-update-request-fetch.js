let displayInConsole = data=>console.log(data);

// GET request to "http://localhost:7000/items"
fetch("http://localhost:80/orders", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
.then(response=>response.json()) // promise of data
.then(renderCustomerInTable)
.catch(e=>console.error(e));

function editable(id) {
  console.log("Edit id of "+id);
  // Toggle every hidden field on/off in our table cells:
  // let field = document.getElementById(`name-${id}`)

//debugger;

     document.getElementById('inputId').value = id;

    let editName = document.getElementById(`edit-orderName-${id}`).value;
    document.getElementById('inputOrderName').value = editName;
  
    let editQty = document.getElementById(`edit-orderQuantity-${id}`).value;
    document.getElementById('inputQty').value = editQty;

    let editPrice = document.getElementById(`edit-price-${id}`).value;
    document.getElementById('inputPrice').value = editPrice;

    let editTotal = document.getElementById(`edit-orderTotal-${id}`).value;
    document.getElementById('inputTotal').value = editTotal;
  
    let editProdId = document.getElementById(`edit-prodId-${id}`).value;
    document.getElementById('inputProdId').value = editProdId;

    let editProdName = document.getElementById(`edit-prodName-${id}`).value;
    document.getElementById('inputProdName').value = editProdName;

    let editCustomerId = document.getElementById(`edit-custId-${id}`).value;
    document.getElementById('inputCustId').value = editCustomerId;

    let editCustomerName = document.getElementById(`edit-custName-${id}`).value;
    document.getElementById('inputCustName').value = editCustomerName;
  
    let editStock = document.getElementById(`edit-stock-${id}`).value;
    document.getElementById('inputStock').value = editStock;

    console.log("orderId: "+id+" OrderName: "+editName+" orderQty: "+editQty +
                " orderTotal: "+editTotal+ " editStock: "+ editStock+" editProdName:"+ 
                editProdName+"editCustomerId: "+editCustomerId+" editCustomerName: "+editCustomerName);
}

function renderCustomerInTable(orderList){
    document.getElementById("orderList-table").hidden = false;
    const tableBody = document.getElementById("order-table-body");
    for(let order of orderList){
        let newRow = document.createElement("tr");

        let edit = `
          <input type='radio' name='edit' value='${order.orderId}' onclick='editable(${order.orderId})'>
          <label for='edit-${order.orderId}'>${order.orderId}</label>
        `; 

        const total = (order.musicProduct.price * order.orderQuantity );


        newRow.innerHTML = `
            <td>${edit}</td>
            <td>
              <span id='readonly-orderName-${order.orderId}'>${order.orderName}</span>
              <input hidden type='text' size='16' id='edit-orderName-${order.orderId}'  value='${order.orderName}'></td>
            <td>
              <span id='readonly-orderQuantity-${order.orderId}'>${order.orderQuantity}</span>
              <input hidden type='text' size='40' id='edit-orderQuantity-${order.orderId}'  value='${order.orderQuantity}'></td>
            <td>
              <span id='readonly-price-${order.orderId}'>${order.musicProduct.price}</span>
              <input hidden type='text' size='20' id='edit-price-${order.orderId}'  value='${order.musicProduct.price}'></td>
            <td>
              <span id='readonly-orderTotal-${order.orderId}'>${total}</span>
              <input hidden type='text' size='14' id='edit-orderTotal-${order.orderId}'  value='${total}'></td>

            <td>
              <span id='readonly-stock-${order.orderId}'>${order.musicProduct.stock}</span>
              <input hidden type='text' size='20' id='edit-stock-${order.orderId}'  value='${order.musicProduct.stock}'></td>

            <td>
              <span id='readonly-prodId-${order.orderId}'>${order.musicProduct.prodId}</span>
              <input hidden type='text' size='20' id='edit-prodId-${order.orderId}'  value='${order.musicProduct.prodId}'></td>
            <td>
              <span id='readonly-prodName-${order.orderId}'>${order.musicProduct.prodName}</span>
              <input hidden type='text' size='20' id='edit-prodName-${order.orderId}'  value='${order.musicProduct.prodName}'></td>
              <td>
              <span id='readonly-custId-${order.orderId}'>${order.customer.customerId}</span>
              <input hidden type='text' size='20' id='edit-custId-${order.orderId}'  value='${order.customer.customerId}'></td>
            <td>
              <span id='readonly-custName-${order.orderId}'>${order.customer.customerName}</span>
              <input hidden type='text' size='20' id='edit-custName-${order.orderId}'  value='${order.customer.customerName}'></td>

        `;
        tableBody.appendChild(newRow);
    }    
}
