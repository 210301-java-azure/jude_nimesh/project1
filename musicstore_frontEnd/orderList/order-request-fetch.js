let displayInConsole = data=>console.log(data);

// GET request to "http://localhost:7000/items"
fetch("http://localhost:80/orders", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
.then(response=>response.json()) // promise of data
.then(renderCustomerInTable)
.catch(e=>console.error(e));

const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
  })

function renderCustomerInTable(orderList){
    document.getElementById("orderList-table").hidden = false;
    const tableBody = document.getElementById("orders-table-body");
    for(let order of orderList){
        let newRow = document.createElement("tr");
        let total = (order.orderQuantity * order.musicProduct.price);

        newRow.innerHTML = `<td>${order.orderId}</td>
        <td>${order.orderName}</td>
        <td>${order.musicProduct.prodName}</td>
        <td>${order.orderQuantity}</td>
        <td>${formatter.format(order.musicProduct.price)}</td>
        <td>${formatter.format(total)}</td>
        <td>${order.customer.customerName}</td>`;
        tableBody.appendChild(newRow);
    }    
}

//        <td>${formatter.format(order.musicProduct.price)}</td>
//<td>${formatter.format(total)}</td>