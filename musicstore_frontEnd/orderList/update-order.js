document.getElementById("update-order-list-form").addEventListener("submit", updateOrder);

function updateOrder(e){
    e.preventDefault();

    const orderId   = document.getElementById("inputId").value;
    const orderName = document.getElementById("inputOrderName").value;
    const orderQty  = document.getElementById("inputQty").value;
    const orderTotal = document.getElementById("inputTotal").value;
    const orderPrice = document.getElementById("inputPrice").value;
    const orderStock = document.getElementById("inputStock").value;
    const custId     = document.getElementById("inputCustId").value;
    const custName   = document.getElementById("inputCustName").value;
    const prodId     = document.getElementById("inputProdId").value;
    const prodName   = document.getElementById("inputProdName").value;

/*
    const newOrder = {"orderId": orderId, "orderName": orderName, "orderQuantity": orderQty, "orderTotal": orderTotal, 
                      "custome": { "customerId": custId,  "customerName": custName },
                       "musicProduct": { "prodId": prodId, "prodName": prodName, 
                                         "stock": orderStock, "stock": orderStock, "price": orderPrice },
                     };
*/
    const newOrder = {"orderId": orderId, "orderName": orderName, "orderQuantity": orderQty, "orderTotal": orderTotal, 
                     "customer": { "customerId": custId   },
                      "musicProduct": { "prodId": prodId },
                    };                     

    //  if Authorization is null, for testing purpose manully set the sessionStorage
    if(sessionStorage.getItem('Authorization' == null )){
        console.log(" token Authorization access status: "+ sessionStorage.getItem("Authorization" ));
        sessionStorage.setItem('Authorization', 'admin-auth-token'); 
    } else {
        console.log(" token is Authorization to access");
    }                    

    // javascript object (newCustomer) -> json -> java object (CustomerList.class)
    ajaxUpdateOrder(orderId, newOrder, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Seletced order list was successfully Updated";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue updating your selected Order list";
}
