document.getElementById("new-order-list-form").addEventListener("submit", addNewOrder);

function addNewOrder(e){
    e.preventDefault();

    //  if Authorization is null, for testing purpose manully set the sessionStorage
    if(sessionStorage.getItem('Authorization' == null )){
        console.log(" token Authorization access status: "+ sessionStorage.getItem("Authorization" ));
        sessionStorage.setItem('Authorization', 'admin-auth-token'); 
    } else {
        console.log(" token is Authorization to access");
    }

    const orderName = document.getElementById("inputOrderName").value;
    const orderQty  = document.getElementById("inputQty").value;

    const prodId    = document.getElementById("inputProductId").value;

    //const prodPrice  = document.getElementById("inputPrice").value;
    
    let orderTotal  = 0;
    //const orderTotal = document.getElementById("inputTotal").value;  
    if( orderQty != null && prodPrice != null ) {
        orderTotal = orderQty * prodPrice; // calculated by Qty X Price
    }
    
    const custId    = document.getElementById("inputCustomerId").value;

    //const prodName  = document.getElementById("inputProductName").value;
    //const prodStock = document.getElementById("inputStock").value;
    //const custName  = document.getElementById("inputCustomerName").value;


    const newOrder  = {"orderName": orderName, "orderQuantity": orderQty, "orderTotal": orderTotal, 
                       "musicProduct": { "prodId": prodId,  "price": prodPrice
                                       //"prodName": prodName, "stock": prodStock 
                                       },
                       "customer": { "customerId": custId 
                                   //"customerName": custName 
                                   }         
                     };          

    // javascript object (newOrder) -> json -> java object (Order.class)
    ajaxCreateOrder(newOrder, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message  = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "New Order list successfully created";
}

function indicateFailure(){
    const message  = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue creating your new Order list";
}

