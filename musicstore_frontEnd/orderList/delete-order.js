document.getElementById("delete-order-list-form").addEventListener("submit", deleteOrderById);

function deleteOrderById(e){
    e.preventDefault();

    const orderId   = document.getElementById("inputId").value;
    console.log(orderId);

    // javascript object (newCustomer) -> json -> java object (CustomerList.class)
    ajaxDeleteOrder(orderId, indicateSuccess, indicateFailure);
    
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Seletced Order was successfully deleted";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue deleting your selected Order";
}
 
    
 

