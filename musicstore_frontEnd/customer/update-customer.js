console.log("Hi");
document.getElementById("update-customer-list-form").addEventListener("submit", updateCustomerById);

function updateCustomerById(e){
    e.preventDefault();
    const customerId    = document.getElementById("inputId").value;
    const customerName  = document.getElementById("inputName").value;
    const customerAddress = document.getElementById("inputAddress").value;
    const customerPhone = document.getElementById("inputPhone").value;
    const customerEmail = document.getElementById("inputEmail").value;
    const customerPass  = document.getElementById("inputPassword").value;
    const userId        = document.getElementById("inputUserId").value;

    const updateCustomerId = customerId;
    const updateCustomer   = { "customerId": customerId, "customerName": customerName, "address": customerAddress, "phone": customerPhone, 
                              "user": { "userId": userId, "email":  customerEmail, "password": customerPass} };
                             
    //  if Authorization is null, for testing purpose manully set the sessionStorage
    if(sessionStorage.getItem('Authorization' == null )){
        console.log(" token Authorization access status: "+ sessionStorage.getItem("Authorization" ));
        sessionStorage.setItem('Authorization', 'admin-auth-token'); 
    } else {
        console.log(" token is Authorization to access");
    }                              
    
    // javascript object (newCustomer) -> json -> java object (CustomerList.class)
    ajaxUpdateCustomer(updateCustomerId, updateCustomer, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Seletced Customer record was successfully Updated";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue updating your selected Customer record";
}
