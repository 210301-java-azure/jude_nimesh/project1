document.getElementById("new-customer-list-form").addEventListener("submit", addNewCustomer);

function addNewCustomer(e){
    e.preventDefault();

    const customerName  = document.getElementById("inputName").value;
    const customerAddress = document.getElementById("inputAddress").value;
    const customerPhone = document.getElementById("inputPhone").value;
    const customerEmail = document.getElementById("inputEmail").value;
    const customerPass  = document.getElementById("inputPassword").value;

    //  if Authorization is null, for testing purpose manully set the sessionStorage
    if(sessionStorage.getItem('Authorization' == null )){
        console.log(" token Authorization access status: "+ sessionStorage.getItem("Authorization" ));
        sessionStorage.setItem('Authorization', 'admin-auth-token'); 
    } else {
        console.log(" token is Authorization to access");
    }

    const newCustomer = {"customerName": customerName, "address": customerAddress, "phone": customerPhone, 
                         "user": { "email":  customerEmail, "password": customerPass} };
    // javascript object (newCustomer) -> json -> java object (CustomerList.class)
    ajaxCreateCustomer(newCustomer, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "New Customer successfully created";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue creating your new Customer record";
}
