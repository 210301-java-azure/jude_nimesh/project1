document.getElementById("delete-customer-list-form").addEventListener("submit", updateCustomerById);

function updateCustomerById(e){
    e.preventDefault();
    const customerId    = document.getElementById("inputId").value;
    console.log(customerId);

    // javascript object (newCustomer) -> json -> java object (CustomerList.class)
    ajaxDeleteCustomer(customerId, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Selected Customer record was successfully deleted";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue deleting your selected Customer record";
}
