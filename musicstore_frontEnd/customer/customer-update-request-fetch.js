let displayInConsole = data=>console.log(data);

// GET request to "http://localhost:7000/items"
fetch("http://localhost:80/customers", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
//fetch("http://20.185.55.223/items", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
.then(response=>response.json()) // promise of data
.then(renderCustomerInTable)
.catch(e=>console.error(e));

function editable(id) {
  console.log("Edit id of "+id);
  // Toggle every hidden field on/off in our table cells:
  // let field = document.getElementById(`name-${id}`)
   document.getElementById("inputId").value = id;

  let editName = document.getElementById(`edit-name-${id}`).value;
  document.getElementById('inputName').value = editName;

  let editAddress = document.getElementById(`edit-address-${id}`).value;
  document.getElementById('inputAddress').value = editAddress;

  let editPhone = document.getElementById(`edit-phone-${id}`).value;
  document.getElementById('inputPhone').value = editPhone;

  let editEmail = document.getElementById(`edit-email-${id}`).value;
  document.getElementById('inputEmail').value = editEmail;

  let editPassword = document.getElementById(`edit-password-${id}`).value;
  document.getElementById('inputPassword').value = editPassword;

  let editUserId = document.getElementById(`edit-userid-${id}`).value;
  document.getElementById('inputUserId').value = editUserId;

  document.getElementById('inputPasswordConf').value = editPassword;

  console.log( editName, editAddress, editPhone,  editEmail, editPassword );
}

function renderCustomerInTable(customerList){
  document.getElementById("customerList-table").hidden = false;
  const tableBody = document.getElementById("customer-table-body");
  for(let cust of customerList){
    let newRow = document.createElement("tr");

    let edit = `
      <input type='radio' name='customer' value='${cust.customerId}' onclick='editable(${cust.customerId})'>
      <label for='edit-${cust.customerId}'>${cust.customerId}</label>
    `; 

    newRow.innerHTML = `
      <td>${edit}</td>
        <td>
          <span id='readonly-name-${cust.customerId}'>${cust.customerName}</span>
          <input hidden type='text' size='16' id='edit-name-${cust.customerId}'  value='${cust.customerName}'></td>
        <td>
        <span id='readonly-address-${cust.customerId}'>${cust.address}</span>
          <input hidden type='text' size='40' id='edit-address-${cust.customerId}'  value='${cust.address}'></td>
        <td>
        <span id='readonly-phone-${cust.customerId}'>${cust.phone}</span>
          <input hidden type='text' size='14' id='edit-phone-${cust.customerId}'  value='${cust.phone}'></td>
        <td>
          <span id='readonly-email-${cust.customerId}'>${cust.user.email}</span>
          <input hidden type='text' size='20' id='edit-email-${cust.customerId}'  value='${cust.user.email}'></td>
        <td>
          <span id='readonly-password-${cust.customerId}'>${cust.user.password}</span>
          <input hidden type='text' size='20' id='edit-password-${cust.customerId}'  value='${cust.user.password}'></td>
        <td>
          <span id='readonly-userid-${cust.customerId}'>${cust.user.userId}</span>
          <input hidden type='text' size='10' id='edit-userid-${cust.customerId}'  value='${cust.user.userId}'></td>
        `;
    tableBody.appendChild(newRow);
  }    
}
