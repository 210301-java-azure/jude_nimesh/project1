let displayInConsole = data=>console.log(data);

// GET request to "http://localhost:7000/items"
fetch("http://localhost:80/customers", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
.then(response=>response.json()) // promise of data
.then(renderCustomerInTable)
.catch(e=>console.error(e));

function renderCustomerInTable(customerList){
    document.getElementById("customerList-table").hidden = false;
    const tableBody = document.getElementById("customer-table-body");
    for(let cust of customerList){
        let newRow = document.createElement("tr");
    
        newRow.innerHTML = `
            <td>${cust.customerId}</td>
            <td>${cust.customerName}</td>
            <td>${cust.address}</td>
            <td>${cust.phone}</td>
            <td>${cust.user.email}</td>
          `;
        tableBody.appendChild(newRow);
    }    

}
