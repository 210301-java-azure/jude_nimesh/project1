let displayInConsole = data=>console.log(data);

// GET request to "http://localhost:7000/items"
fetch("http://localhost:80/items", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
//fetch("http://20.185.55.223/items", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
.then(response=>response.json()) // promise of data
.then(renderItemsInTable)
.catch(e=>console.error(e));

const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
  })

function renderItemsInTable(itemsList){
    document.getElementById("items-table").hidden = false;
    const tableBody = document.getElementById("items-table-body");
    for(let item of itemsList){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `
            <td>${item.prodId}</td>
            <td>${item.prodName}</td>
            <td>${item.stock}</td>
            <td>${formatter.format(item.price)}</td>
            <td>${item.section}</td>`;
        tableBody.appendChild(newRow);
    }    
}
