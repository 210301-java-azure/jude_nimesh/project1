document.getElementById("new-product-item-form").addEventListener("submit", addNewItem);

function addNewItem(e){
    e.preventDefault();
    const itemName = document.getElementById("inputProduct").value;
    const itemSection = document.getElementById("inputSection").value;
    const itemStock = document.getElementById("inputStock").value;
    const itemPrice = document.getElementById("inputPrice").value;

    const newItem = {"productName": itemName, "section": itemSection, 
                     "stock": itemStock, "price": itemPrice };


    // javascript object (newItem) -> json -> java object (MusicProduct.class)
    ajaxCreateProduct(newItem, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "New Product item successfully created";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue creating your new Product item";
}

