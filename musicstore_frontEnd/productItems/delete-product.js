document.getElementById("delete-product-item-form").addEventListener("submit", deleteProductById);

function deleteProductById(e){
    e.preventDefault();
    const productId    = document.getElementById("inputId").value;

    // javascript object (newCustomer) -> json -> java object (CustomerList.class)
    ajaxDeleteProduct(productId, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Seletced Product Item was successfully deleted";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue deleting your selected Product Item";
}
