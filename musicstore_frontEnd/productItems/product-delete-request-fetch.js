let displayInConsole = data=>console.log(data);

// GET request to "http://localhost:7000/items"
fetch("http://localhost:80/items", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
//fetch("http://20.185.55.223/items", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
.then(response=>response.json()) // promise of data
.then(renderItemsInTable)
.catch(e=>console.error(e));

const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
  })

function editable(id) {
    console.log("Edit id of "+id);
    // Toggle every hidden field on/off in our table cells:
     let field = document.getElementById(`name-${id}`)
     document.getElementById("inputId").value = id;
}

function renderItemsInTable(itemsList){
    document.getElementById("product-item-table").hidden = false;
    const tableBody = document.getElementById("product-table-body");
    for(let item of itemsList){
        let newRow = document.createElement("tr");

        let edit = `
            <input type='radio' name='product' value='${item.prodId}' onclick='editable(${item.prodId})'>
            <label for='edit-${item.prodId}'>${item.prodId}</label>
        `;

        newRow.innerHTML = `
        <td>${edit}</td>
        <td>
          <span id='readonly-prodName-${item.prodId}'>${item.prodName}</span>
          <input hidden type='text' size='30' id='edit-prodName-${item.prodId}'  value='${item.prodName}'></td>
        <td>
          <span id='readonly-stock-${item.prodId}'>${item.stock}</span>
          <input hidden type='text' size='10' id='edit-stock-${item.prodId}'  value='${item.stock}'></td>
        <td>
          <span id='readonly-price-${item.prodId}'>${formatter.format(item.price)}</span>
          <input hidden type='text' size='10' id='edit-price-${item.prodId}'  value='${formatter.format(item.price)}'></td>
        <td>
          <span id='readonly-section-${item.prodId}'>${item.section}</span>
          <input hidden type='text' size='10' id='edit-section-${item.prodId}'  value='${item.section}'></td>

        `;
    tableBody.appendChild(newRow);
  }    
}
