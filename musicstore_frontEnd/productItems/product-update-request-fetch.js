let displayInConsole = data=>console.log(data);

// GET request to "http://localhost:7000/items"
fetch("http://localhost:80/items", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
//fetch("http://20.185.55.223/items", {headers:{"Authorization":"admin-auth-token"}}) // promise of a response
.then(response=>response.json()) // promise of data
.then(renderCustomerInTable)
.catch(e=>console.error(e));


function editable(id) {
    console.log("Edit id of "+id);
    // Toggle every hidden field on/off in our table cells:
    // let field = document.getElementById(`name-${id}`)

    document.getElementById('inputId').value = id

    let editName = document.getElementById(`edit-name-${id}`).value;
    document.getElementById('inputProduct').value = editName;

    let editStock = document.getElementById(`edit-stock-${id}`).value;
    document.getElementById('inputStock').value = editStock;
  
    let editPrice = document.getElementById(`edit-price-${id}`).value;
    document.getElementById('inputPrice').value = editPrice;
  
    let editSection = document.getElementById(`edit-section-${id}`).value;
    document.getElementById('inputSection').value = editSection;

  
    console.log( id, editName, editStock, editPrice,  editSection );
}

function renderCustomerInTable(ProductList){
    document.getElementById("product-item-table").hidden = false;
    const tableBody = document.getElementById("product-table-body");
    for(let prod of ProductList){
        let newRow = document.createElement("tr");

        let edit = `
        <input type='radio' name='product' value='${prod.prodId}' onclick='editable(${prod.prodId})'>
        <label for='edit-${prod.prodId}'>${prod.prodId}</label>
    `;

        newRow.innerHTML = `
            <td>${edit}</td>
            <td>
              <span id='readonly-name-${prod.prodId}'>${prod.prodName}</span>
              <input hidden type='text' size='16' id='edit-name-${prod.prodId}'  value='${prod.prodName}'></td>
            <td>
              <span id='readonly-stock-${prod.prodId}'>${prod.stock}</span>
              <input hidden type='text' size='40' id='edit-stock-${prod.prodId}'  value='${prod.stock}'></td>
            <td>
              <span id='readonly-price-${prod.prodId}'>${prod.price}</span>
              <input hidden type='text' size='10' id='edit-price-${prod.prodId}'  value='${prod.price}'></td>
            <td>
              <span id='readonly-section-${prod.prodId}'>${prod.section}</span>
              <input hidden type='text' size='20' id='edit-section-${prod.prodId}'  value='${prod.section}'></td>
        `;
        tableBody.appendChild(newRow);
    }    




}
