document.getElementById("update-product-item-form").addEventListener("submit", updateProductById);

function updateProductById(e){
    e.preventDefault();
    const productId    = document.getElementById("inputId").value;
    const productName  = document.getElementById("inputProduct").value;
    const productStock = document.getElementById("inputStock").value;
    const productPrice = document.getElementById("inputPrice").value;
    const productSection = document.getElementById("inputSection").value;

    const updateProduct   = { "prodId": productId, "prodName": productName, 
                              "stock":  productStock, "price": productPrice, 
                              "section": productSection,
                            };                         
    
    // javascript object (newCustomer) -> json -> java object (CustomerList.class)
    ajaxUpdateProduct(productId, updateProduct, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "Seletced Product Item was successfully Updated";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue updating your selected Product Item";
}
