function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken){
    const xhr = new XMLHttpRequest(); // ready state 0 
    console.log("storagesession status received/sent: ", authToken )
    
    //Since storage-session does not work properly temp solution
    if(sessionStorage.getItem('Authorization') == null ){
        console.log(" token Authorization access status: "+ sessionStorage.getItem("Authorization" ));
        sessionStorage.setItem('Authorization', 'admin-auth-token'); 
    } else {
        console.log(" token is Authorization to access");
    }
    authToken = sessionStorage.getItem("Authorization");


    xhr.open(method, url); // ready state 1  //"GET", "http://localhost:7000/items"
    if (authToken){
        xhr.setRequestHeader("Authorization", authToken);
    }

    console.log("authToken[Authorization] sent:"+ authToken)
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if(body){
        xhr.send(body);
    } else {
        xhr.send(); // ready state 2,3,4 follow
    }
}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}

function sendAjaxPUT(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("PUT", url, body, successCallback, failureCallback, authToken);
}

function sendAjaxDELETE(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("DELETE", url, body, successCallback, failureCallback, authToken);
}
//------------------------------------------------------------------------------
//Login to Music Store and seet Token key Authorization
function ajaxLogin(username, password, successCallback, failureCallback){
    // username=jnimesh@gmail.com&password=password
    const payload = `username=${username}&password=${password}`; // "username="+username+"&password"+password
    sendAjaxPost("http://localhost:80/login", payload, successCallback, failureCallback);
}
//------------------------------------------------------------------------------

//Music Product created
function ajaxCreateProduct(item, successCallback, failureCallback){
    const itemJson = JSON.stringify(item);
    const auth = sessionStorage.getItem("Authorization");
    console.log("Auth token ststus: " + auth );
    console.log("Show Create data itemJson: "+itemJson );
    sendAjaxPost(`http://localhost:80/items/`, itemJson, successCallback, failureCallback, auth);
}

//Music Product updated
function ajaxUpdateProduct(prodId, product, successCallback, failureCallback){
    const prodIdJson = JSON.stringify(product);
    const auth = sessionStorage.getItem("Authorization");
    console.log("Auth token ststus: " + auth );
    console.log("Show Create data prodId: "+prodIdJson );
    sendAjaxPUT(`http://localhost:80/items/${prodId}`, prodIdJson, successCallback, failureCallback, auth);
}

//Music Product deleted
function ajaxDeleteProduct(prodId, successCallback, failureCallback){
    //const itemJson = JSON.stringify(prodId);
    const auth = sessionStorage.getItem("Authorization");
    console.log("Auth token ststus: " + auth );
    console.log("Show Create data customer: "+prodId );
    sendAjaxDELETE(`http://localhost:80/items/${prodId}`, undefined, successCallback, failureCallback, auth);
}
/*------------------------------------------------------------------------*/

//Music Customer created New
function ajaxCreateCustomer(customer, successCallback, failureCallback){
    const customerJson = JSON.stringify(customer);

    sessionStorage.setItem('Authorization', 'admin-auth-token');
    const auth = sessionStorage.getItem("Authorization");
    console.log("Show Create data customer: "+customerJson );
    console.log("Auth token ststus: " + auth );
    sendAjaxPost(`http://localhost:80/customers/`, customerJson, successCallback, failureCallback, auth);
}

//Music Customer updated
function ajaxUpdateCustomer(customerId, customer, successCallback, failureCallback){
    const customerJson = JSON.stringify(customer);

    const auth = sessionStorage.getItem("Authorization");
    console.log("Show data: customerJson "+customerJson );
    console.log("Auth token ststus: " + auth );
    sendAjaxPUT(`http://localhost:80/customers/${customerId}`, customerJson, successCallback, failureCallback, auth);
}

//Music Customer deleted
function ajaxDeleteCustomer(customerId, successCallback, failureCallback){
    //const customerJson = JSON.stringify(customer);
    const auth = sessionStorage.getItem("Authorization");
    console.log("Auth token ststus: " + auth );
    console.log("Show data: customerId "+customerId );
    sendAjaxDELETE(`http://localhost:80/customers/${customerId}`, undefined, successCallback, failureCallback, auth);
}
/*------------------------------------------------------------------------*/

//Music Order created
function ajaxCreateOrder(order, successCallback, failureCallback){
    const orderJson = JSON.stringify(order);
    const auth = sessionStorage.orderId("Authorization");
    console.log("Auth token ststus: " + auth );
    console.log("Show data: orderJson "+orderJson );
    sendAjaxPost("http://localhost:80/orders/", orderJson, successCallback, failureCallback, auth);
}

//Music Order updated
function ajaxUpdateOrder(orderId, order, successCallback, failureCallback){
    const orderJson = JSON.stringify(order);
    const auth = sessionStorage.getItem("Authorization");
    console.log("Auth token ststus: " + auth );
    console.log("Show data: orderJson "+orderJson );
    sendAjaxPUT(`http://localhost:80/orders/${orderId}`, orderJson, successCallback, failureCallback, auth);
}

//Music Order deleted
function ajaxDeleteOrder(orderId, successCallback, failureCallback){
    //const orderJson = JSON.stringify(orderId);
    const auth = sessionStorage.getItem("Authorization");
    console.log("Auth token received: " + auth );
    console.log("Show data: orderJson "+orderJson );
    sendAjaxDELETE(`http://localhost:80/orders/${orderId}`, undefined, successCallback, failureCallback, auth);
}

