package dev.nimesh.controllers;

import dev.nimesh.models.CustomerList;
import dev.nimesh.models.OrderList;
import dev.nimesh.models.MusicProduct;
import dev.nimesh.services.MusicStoreService;
import dev.nimesh.services.OrderListService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ProductController {

    private Logger logger = LoggerFactory.getLogger(ProductController.class);
    private MusicStoreService service = new MusicStoreService();
    private OrderListService  serviceOrder = new OrderListService();

    // this method handles GET /items
    public void handleGetProductRequest(Context ctx){
        logger.info("Product Items all listing");

        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
            logger.info("getting items in price range");
            ctx.json(service.getItemsInRange(minPrice, maxPrice));
        } else { // /items with no query params
            //ctx.result(data.getAllItems().toString());}
            logger.info("getting all items");
            ctx.json(service.getAll()); // json method converts object to JSON
        }
    }

    public void handleGetProductByIdRequest(Context ctx) {
        String idString = ctx.pathParam("prodId");

        logger.info("Product Items idString="+idString);
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            MusicProduct item = service.getById(idInput);
            if (item == null) {
                //                ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: " + idInput);
                ctx.json(item);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewProduct(Context ctx){
        MusicProduct item = ctx.bodyAsClass(MusicProduct.class);
        logger.info("adding new product item: "+item);

        service.add(item);
        ctx.status(201);
    }

    public void handleDeleteProductById(Context ctx){
        String idString = ctx.pathParam("prodId");

        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            MusicProduct item = service.getById(idInput);
            List<OrderList> itemsExist = serviceOrder.getAll();
            for (OrderList orderItemEx : itemsExist) {
                logger.info("TEST:1000: item.getProdId(): {} and orderItemEx.getMusicProduct().getProdId(): ",
                        item.getProdId(),orderItemEx.getMusicProduct().getProdId());
                if (item.getProdId() == (orderItemEx.getMusicProduct().getProdId())) {
                    logger.info("TEST:1100: item.getProdId(): {} and orderItemEx.getMusicProduct().getProdId(): ",
                            item.getProdId(),orderItemEx.getMusicProduct().getProdId());
                    logger.info(" Product items already exist in the order database:" + orderItemEx.getMusicProduct().getProdId());
                    int order_id = orderItemEx.getOrderId();
                    logger.info(" Product item is deleted from orderlist inorder to remove it from the product list " + order_id);
                    serviceOrder.delete(order_id);
                }
            }
            logger.info("checking item got captured in int id: " + item); //test

            int prod_id = item.getProdId();
            logger.info("checking id got captured in int id: " + prod_id); //test
            if( prod_id != 0){
                logger.info(" Product item is deleted from MusicProduct list prod_id: "+ prod_id);
                service.delete(prod_id);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateProductById(Context ctx) {
        MusicProduct newItem = ctx.bodyAsClass(MusicProduct.class);
        logger.info("replacing a new product item: "+newItem);

        String idString = ctx.pathParam("prodId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("checking id got captured in int id: " + idInput); //test

            MusicProduct item = service.getById(idInput);
            if (item == null) {
                // ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No product item found with provided ID: " + idInput);
            } else {
                logger.info("Before updating an existing product item: "+item);

                service.update(item, newItem);
                logger.info("After Updating to a new product item(on same index): "+newItem);
                ctx.json(newItem);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

}
