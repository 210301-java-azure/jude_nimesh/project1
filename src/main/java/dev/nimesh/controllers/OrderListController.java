package dev.nimesh.controllers;

import dev.nimesh.models.CustomerList;
import dev.nimesh.models.MusicProduct;
import dev.nimesh.models.OrderList;
import dev.nimesh.models.UserList;
import dev.nimesh.services.MusicStoreService;
import dev.nimesh.services.OrderListService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderListController {

    private Logger logger = LoggerFactory.getLogger(OrderListController.class);
    private OrderListService service = new OrderListService();

    // this method handles GET /items
    public void handleGetOrderRequest(Context ctx){
        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
            logger.info("getting items in price range");
            // ctx.json(service.getItemsInRange(minPrice, maxPrice));
        } else { // /items with no query params
            //ctx.result(data.getAllItems().toString());}
            logger.info("getting all items");
            ctx.json(service.getAll()); // json method converts object to JSON
        }
    }

    public void handleGetOrderByIdRequest(Context ctx) {
        String idString = ctx.pathParam("orderId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            OrderList order = service.getById(idInput);
            if (order == null) {
                //ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: " + idInput);
                ctx.json(order);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewOrder(Context ctx){
        //OrderList order = ctx.bodyAsClass(OrderList.class);

        int qty = 0, order_id= 0, cust_id=0, prod_id=0;
        double total = 0;
        String order_name = ctx.formParam("order_name");

        //cover qty string to int
        String qtyStr   = ctx.formParam("qty");
        if(qtyStr.matches("^\\d+$")){
            qty = Integer.parseInt(qtyStr);
            logger.info("checking qtyStr got captured in int qty: " + qty); //test
        } else {
            throw new BadRequestResponse("input \""+qtyStr+"\" cannot be parsed to an int");
        }

        //convert total string to an double
        String totalStr      = ctx.formParam("total");
        if(totalStr.matches("^\\d+$")){
            total = Double.parseDouble(totalStr);
            logger.info("checking totalStr got captured in double total: " + total); //test
        } else {
            throw new BadRequestResponse("input \""+totalStr+"\" cannot be parsed to an double");
        }

        // convert cust_id string to an int
        String cust_idStr = ctx.formParam("cust_id");
        if(cust_idStr.matches("^\\d+$")){
            cust_id = Integer.parseInt(cust_idStr);
            logger.info("checking qtyStr got captured in int qty: " + qty); //test
        } else {
            throw new BadRequestResponse("input \""+cust_idStr+"\" cannot be parsed to an int");
        }

        // convert cust_id string to an int
        String prod_idStr    = ctx.formParam("prod_id");
        if(prod_idStr.matches("^\\d+$")){
            prod_id = Integer.parseInt(prod_idStr);
            logger.info("checking prod_idStr got captured in int prod_id: " + prod_id); //test
        } else {
            throw new BadRequestResponse("input \""+prod_idStr+"\" cannot be parsed to an int");
        }

        CustomerList customer = new CustomerList(cust_id);
        MusicProduct item     = new MusicProduct(prod_id);

        OrderList orderList = new OrderList( order_id,order_name, qty, total, customer, item);
        logger.info("creating a new order: "+orderList);
        service.add(orderList);
        ctx.status(201);
    }

    public void handleDeleteOrderById(Context ctx){
        String idString = ctx.pathParam("orderId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: "+idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateOrderById(Context ctx) {
        OrderList newOrder = ctx.bodyAsClass(OrderList.class);
        logger.info("replacing a new order: "+newOrder);

        String idString = ctx.pathParam("orderId");
        logger.info("TEST:1500 String value - idString: "+ idString);
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("checking id got captured in int id: " + idInput); //test

            OrderList order = service.getById(idInput);
            logger.info("TEST:3000:A older order id: ", order.getOrderId() );
            logger.info("TEST:3000:B older order: ", order.toString() );

            if (order.getOrderId() == 0) {
                // ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("Before updating an existing item: "+order);

                service.update(order, newOrder);
                logger.info("After Updating to a new order(on same index): "+newOrder);
                logger.info("old order(on same index): "+order);

                ctx.json(newOrder);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

}
