package dev.nimesh.controllers;

//import dev.nimesh.models.MusicProduct;
//import dev.nimesh.models.OrderList;
import dev.nimesh.models.UserList;
import dev.nimesh.services.UserService;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);
    private UserService service = new UserService();
    private UserList userList   = new UserList();

    // this method handles GET /items
    public void handleGetUserRequest(Context ctx){

       // /users with no query params
       //ctx.result(data.getAllUsers().toString());}
       logger.info("getting all users");
         ctx.json(service.getAll()); // json method converts object to JSON
    }

    public void handleGetUserInfo(Context ctx){

        // /users with no query params
        //ctx.result(data.getAllUsers().toString());}
        logger.info("getting all users");
        ctx.json(service.getAll()); // json method converts object to JSON
    }

    public void handleGetUserByIdRequest(Context ctx) {
        String idString = ctx.pathParam("userId");
        logger.info("UserList user idString="+idString);

        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            UserList user = service.getUserListById(idInput);
            if (user == null) {
                //ctx.status(404);
                logger.warn("no user present with id: " + idInput);
                throw new NotFoundResponse("No user found with provided ID: " + idInput);
            } else {
                logger.info("getting user with id: " + idInput);
                ctx.json(user);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewUser(Context ctx){
        // getting params from what would be a form submission (Content-Type: application/x-www-form-urlencoded)
        String email = ctx.formParam("email");
        String pass  = ctx.formParam("password");

        logger.info(email +" user attempted login");
        userList =  new UserList( email, pass);
        //UserList user = ctx.   bodyAsClass(UserList.class);
        logger.info("adding new email: "+email+" & password:"+pass );
        service.add(userList);

        //service.add(user);
        ctx.status(201);
    }

    public void handleDeleteUserById(Context ctx){
        String idString = ctx.pathParam("userId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting user record with an id: "+idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateUserById(Context ctx) {
        UserList newUser = ctx.bodyAsClass(UserList.class);
        logger.info("replacing with a new user: "+newUser);

        String idString = ctx.pathParam("userId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("checking user id got captured in int id: " + idInput); //test

            UserList user = service.getUserListById(idInput);
            if (user == null) {
                // ctx.status(404);
                logger.warn("no user present with id: " + idInput);
                throw new NotFoundResponse("No user found with provided ID: " + idInput);
            } else {
                logger.info("Before updating an existing user: "+user);

                service.update(user, newUser);
                logger.info("After Updating to a new user(on same index): "+newUser);
                ctx.json(newUser);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

}
