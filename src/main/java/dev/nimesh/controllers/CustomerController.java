package dev.nimesh.controllers;

import dev.nimesh.models.CustomerList;
import dev.nimesh.models.MusicProduct;
import dev.nimesh.models.OrderList;
import dev.nimesh.models.UserList;
import dev.nimesh.services.CustomerService;
import dev.nimesh.services.OrderListService;
import dev.nimesh.services.UserService;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CustomerController {

    private Logger logger = LoggerFactory.getLogger(CustomerController.class);
    private CustomerService service = new CustomerService();
    private UserService serviceUser = new UserService();
    private OrderListService serviceOrder = new OrderListService();


    // this method handles GET /items
    public void handleGetCustomerRequest(Context ctx){
        //ctx.result(data.getAllCustomers().toString());}
        logger.info("getting all customers");
        ctx.json(service.getAll()); // json method converts object to JSON
    }

    public void handleGetCustomerByIdRequest(Context ctx) {
        String idString = ctx.pathParam("customerId");
        logger.info("CustomerList customer idString="+idString);

        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            CustomerList customer = service.getCustomerListById(idInput);
            logger.info("600: Customer ID: "+ idInput+"\n"+customer.toString());
            if (customer == null) {
                //                ctx.status(404);
                logger.warn("no customer present with id: " + idInput);
                throw new NotFoundResponse("No customer found with provided ID: " + idInput);
            } else {
                logger.info("getting customer with id: " + idInput);
                ctx.json(customer);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewCustomer(Context ctx) {
        CustomerList newCustomer = ctx.bodyAsClass(CustomerList.class);
        logger.info("Creating a new Customer: " + newCustomer.toString());
        UserList user = new UserList();

        String cust_name = newCustomer.getCustomerName();
        String address = newCustomer.getAddress();
        String phone = newCustomer.getPhone();
        String email = newCustomer.getUser().getEmail();
        String password = newCustomer.getUser().getPassword();

        user = new UserList(email, password);
        CustomerList customer = new CustomerList(0, cust_name, address, phone, user);
        boolean customerNotExist = true;

        List<UserList> userExist = serviceUser.getAll();
        for (UserList userCustEx : userExist) {
            logger.info("TEST:1000: newCustomer.getUserId(): {} and userCustEx.getUserId(): ",
                    customer.getUser().getUserId(), userCustEx.getUserId());

            if (customer.getUser().getUserId() == (userCustEx.getUserId())) {
                logger.info("TEST:1100: newCustomer.getUserId(): {} and userCustEx.getUserId(): ",
                        customer.getUser().getUserId(), userCustEx.getUserId());

                logger.info(" Customer items already exist in the order database:" + userCustEx.getUserId());
                int user_id = userCustEx.getUserId();
                logger.warn("customerList already exist with name: " + cust_name);
                customerNotExist = false;

            }
            if (customerNotExist) {

                logger.info("Add a new customer user to table list ", user.toString());

                UserList newUser = serviceUser.add(user);

                logger.info("Added a new customer user to table list2 ", newUser.toString());
                logger.info("get new user_id placed on table list3 ", newUser.getUserId());

                int user_id = newUser.getUserId();
                logger.info("newly added user_id to the table list ", user_id);

                logger.info("Add a new customer {} to table list ", cust_name);
                service.add(customer);
                ctx.status(201);
                break;
            }
        }
    }
/*
    private int checkUserExist(UserList newUser) {
        List<UserList> userExist = serviceUser.getAll();
        int user_id = 0;
        for( UserList user : userExist) {
            if(user.getEmail() == newUser.getEmail()) {
                user_id = user.getUserId();
                logger.warn("New Customer Entry already exist in the database.\n");
                 return user_id;
            }
        }
        return user_id;
      }
 */
    //----------------------------------------------------------------------

//-------------------------------------------------------------------------------------------
    public void handleDeleteCustomerById(Context ctx){
        String idString = ctx.pathParam("customerId");

        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            CustomerList customer = service.getCustomerListById(idInput);
            logger.info("checking customer got captured in int id: " + customer); //test

            List<OrderList> customerExist = serviceOrder.getAll();
            for (OrderList orderCustEx : customerExist) {
                logger.info("TEST:1000: item.getProdId(): {} and orderItemEx.getMusicProduct().getProdId(): ",
                        customer.getCustomerId(),orderCustEx.getCustomer().getCustomerId());
                if (customer.getCustomerId() == (orderCustEx.getCustomer().getCustomerId())) {
                    logger.info("TEST:1100: item.getProdId(): {} and orderItemEx.getCustomer().getCustomerId(): ",
                            customer.getCustomerId(),orderCustEx.getCustomer().getCustomerId());
                    logger.info(" Customer items already exist in the order database:" + orderCustEx.getCustomer().getCustomerId());
                    int order_id = orderCustEx.getOrderId();
                    logger.info(" Customer is deleted from orderlist inorder to remove it from the Customerlist " + order_id);
                    serviceOrder.delete(order_id);
                }
            }

            // if customer is deleted, then customer have to be deleted from the userlist
            service.delete(idInput);
            try {
                if( customer.getUser().getUserId() != 0) {
                    int user_id = customer.getUser().getUserId();
                    logger.info("checking id got captured in int id in the UserList: " + user_id); //test
                    serviceUser.delete(user_id);
                }
            } catch (NullPointerException e) {
                logger.warn("user Id is null:"+ e.getStackTrace());

            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateCustomerById(Context ctx) {
        CustomerList newCustomer = ctx.bodyAsClass(CustomerList.class);
        logger.info("replacing a new item: "+newCustomer);

        String idString = ctx.pathParam("customerId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("checking id got captured in int id: " + idInput); //test

            CustomerList customer = service.getCustomerListById(idInput);
            if (customer == null) {
                // ctx.status(404);
                logger.warn("no customer present with id: " + idInput);
                throw new NotFoundResponse("No customer found with provided ID: " + idInput);
            } else {
                logger.info("Before updating an existing customer: "+customer);

                service.update(customer, newCustomer);
                logger.info("After Updating to a new user(on same index): "+newCustomer);
                ctx.json(newCustomer);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }
}
