package dev.nimesh.controllers;

import dev.nimesh.models.UserList;
import dev.nimesh.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);
    UserService service = new UserService();

    public void authenticateLogin(Context ctx){
        String username = ctx.formParam("username");  //(Content-Type: application/x-www-form-urlencoded)
        String password = ctx.formParam("password");

        logger.info("username: {} and password: {}", username, password);
        UserList user = service.getUserNamePassword(username);
        logger.info("{} attempted login", user);
        if(user!=null && username.equals(user.getEmail())){
        //if(user!=null && username.equals("jnimesh@gmail.com")){

            if(password!=null && password.equals(user.getPassword())){
            //if(password!=null && password.equals("password")){
                logger.info("successful login");
                // send back token
                ctx.header("Authorization", "admin-auth-token");
                ctx.status(200);
                return;
            }
            throw new UnauthorizedResponse("Incorrect password");

        }
        // invoke service method checking for user
        // if I get back a user send a token with user ID and user role
        // else throw Unauthorized
        throw new UnauthorizedResponse("Username not recognized");
    }

    public void authorizeToken(Context ctx){
        logger.info("attempting to authorize token");

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String authHeader = ctx.header("Authorization");
        logger.info("authHeader info: " +authHeader);
        if(authHeader!=null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.info("authHeader info2: " +authHeader);
            logger.warn("improper authorization");
            throw new UnauthorizedResponse();
        }
    }

}