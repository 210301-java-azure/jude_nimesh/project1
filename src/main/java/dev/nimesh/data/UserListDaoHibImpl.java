package dev.nimesh.data;

import dev.nimesh.models.UserList;
import dev.nimesh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


public class UserListDaoHibImpl implements UserListDao{


    private final Logger logger = LoggerFactory.getLogger(UserListDaoHibImpl.class);

    @Override
    public List<UserList> getAllUsers() {
        try(Session s = HibernateUtil.getSession()){
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from market_item'
            List<UserList> users = s.createQuery("from UserList", UserList.class).list();
            return users;
        }
    }

    @Override
    public UserList getUserById(int id) {
        try(Session s = HibernateUtil.getSession()){
//            UserList user = s.load(MusicProduct.class,id);
            UserList user = s.get(UserList.class, id);
//            logger.info("got item from the database" + user);
            return user;
        }
    }

    @Override
    public UserList getUserByEmail(String email) {
        try(Session s = HibernateUtil.getSession()){
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from market_item'
            List<UserList> users = s.createQuery("from UserList", UserList.class).list();
            for(UserList user : users) {
                logger.info("Email: "+ user.getEmail()+" password: "+ user.getPassword());
                if(email.equals(user.getEmail())) { return user; }
            }
            return null;
        }
    }


    @Override
    public UserList addNewUser(UserList user) {
        try(Session s = HibernateUtil.getSession()) {
            boolean userNotExist = true;
            // check if user already exist
            List<UserList> usersExist = s.createQuery("from UserList", UserList.class).list();
            for(UserList userEx : usersExist) {
                logger.info("Email: " + user.getEmail() + " password: " + user.getPassword());
                logger.info("TEST:900: customer.getUser().getEmail() "+user.getEmail());
                logger.info("TEST:930: userEx.getUser().getEmail() "+userEx.getEmail());

                logger.info("Email: "+ user.getEmail()+" password: "+ user.getPassword());
                if(user.getEmail().equals(userEx.getEmail())) {
                    logger.info("New customer already exist in the database:"+userEx.getEmail() );
                    userNotExist = false;
                    //return null;
                }
                logger.info("TEST:1000 customerNotExist:"+userNotExist);
            }
            logger.info("TEST:1010 customerNotExist:"+userNotExist);

            if( userNotExist) {
//              s.persist(user);
                int id = (int) s.save(user);
                user.setUserId(id);
                Transaction tx = s.beginTransaction();

                logger.info("added new user with id:" + id);
                tx.commit();
                return user;
            }
        }
        return null;
    }

    @Override
    public void deleteUser(int id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(new UserList(id));
            tx.commit();
        }
    }

    @Override
    public UserList updateNewUser(UserList oldUser, UserList newUser) {
        int  id = oldUser.getUserId();
        String  email = newUser.getEmail();
        String  password = newUser.getPassword();
        logger.info("Password info: " + password);
        try (Session s = HibernateUtil.getSession()) {
            UserList user = s.get(UserList.class, id);
            logger.info("100 old user id {} which need to replace: {}", id, password);

            Transaction tx = s.beginTransaction();
            newUser.setUserId(id);

            logger.info("110 new user id {}  email {} which  to replace with: {}", id, email, newUser.toString());

            s.merge(newUser);
            logger.info("update new user with same id:" + id);
            logger.info("update new user info:" + newUser);

            //s.getTransaction().commit();
            tx.commit();
            return newUser;
        }
    }
}

