package dev.nimesh.data;

import dev.nimesh.models.MusicProduct;

import java.util.List;

public interface MusicProductDao {

    public List<MusicProduct> getAllProducts();
    public MusicProduct getProductById(int id);
    public MusicProduct addNewProduct(MusicProduct item);
    public MusicProduct updateProduct(MusicProduct oldItem, MusicProduct newItem);
    void deleteProduct(int id);
    
    public List<MusicProduct> getItemsInRange(double min, double max);
    public List<MusicProduct> getItemsWithMaxPrice( double maxPrice);
    public List<MusicProduct> getItemsWithMinPrice( double minPrice);


}
