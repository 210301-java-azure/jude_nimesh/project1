package dev.nimesh.data;

import dev.nimesh.models.OrderList;
import dev.nimesh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


public class OrderListDaoHibImpl implements OrderListDao{

    private final Logger logger = LoggerFactory.getLogger(OrderListDaoHibImpl.class);

    @Override
    public List<OrderList> getAllOrders() {
        try(Session s = HibernateUtil.getSession()){
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from order_list'
            List<OrderList> orders = s.createQuery("from OrderList", OrderList.class).list();
            return orders;
        }
    }

    @Override
    public OrderList getOrderById(int order_id) {
        try(Session s = HibernateUtil.getSession()){
//            OrderList order = s.load(OrderList.class, order_id);
            OrderList order = s.get(OrderList.class, order_id);
//            logger.info("got order from the database" + order);
            return order;
        }
    }

    @Override
    public OrderList addNewOrder(OrderList order) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            s.persist(order);
            int order_id = (int) s.save(order);
            order.setOrderId(order_id);

            logger.info("added new order with id:"+order_id);
            tx.commit();
            return order;
        }
    }

    @Override
    public void deleteOrder(int order_id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(new OrderList(order_id));
            tx.commit();
        }
    }

    @Override
    public OrderList updateOrder(OrderList oldOrder, OrderList newOrder) {
        //int order_id = oldOrder.getMusicProduct().getProdId();
        int order_id = oldOrder.getOrderId();
        int prod_id  = oldOrder.getMusicProduct().getProdId();
        int cust_id  = oldOrder.getCustomer().getCustomerId();



        logger.info("TEST:2000:A prod_id: "+ prod_id);
        logger.info("TEST:2000:B cust_id: "+ cust_id);
        logger.info("TEST:2000:C order_id: "+ order_id);

        logger.info("TEST:2000:B oldOrder: "+ oldOrder.toString());


        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();



            //OrderList order = s.get(OrderList.class, order_id);
            //logger.info("old product id {} which need to replace: { }", order_id, order.toString());


            newOrder.setOrderId(order_id);
            newOrder.setMusicProduct(oldOrder.getMusicProduct());
            newOrder.setCustomer(oldOrder.getCustomer());
            s.merge(newOrder);
            logger.info("update new order with same id:" + order_id);
            logger.info("update new order info:" + newOrder.toString());

            tx.commit();
            return newOrder;
        }
    }
}
