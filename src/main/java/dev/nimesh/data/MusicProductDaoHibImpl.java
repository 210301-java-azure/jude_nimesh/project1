package dev.nimesh.data;

import dev.nimesh.models.MusicProduct;
import dev.nimesh.models.OrderList;
import dev.nimesh.util.ConnectionUtil;
import dev.nimesh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class MusicProductDaoHibImpl implements MusicProductDao {


    private final Logger logger = LoggerFactory.getLogger(MusicProductDaoHibImpl.class);

    @Override
    public List<MusicProduct> getAllProducts() {
        try(Session s = HibernateUtil.getSession()){
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from market_item'
            List<MusicProduct> items = s.createQuery("from MusicProduct", MusicProduct.class).list();
            return items;
        }
    }

    @Override
    public MusicProduct getProductById(int prod_id) {
        try(Session s = HibernateUtil.getSession()){
//            MusicProduct item = s.load(MusicProduct.class,id);
            MusicProduct item = s.get(MusicProduct.class, prod_id);
//            logger.info("got item from the database" + item);
            return item;
        }
    }

    @Override
    public List<MusicProduct> getItemsInRange(double min, double max) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<MusicProduct> cq = cb.createQuery(MusicProduct.class);

            Root<MusicProduct> root = cq.from(MusicProduct.class);
            cq.select(root);

            cq.where(cb.between(root.get("price"), min, max));

            Query<MusicProduct> query = s.createQuery(cq);
            return query.list();
        }
    }

    @Override
    public List<MusicProduct> getItemsWithMaxPrice(double max) {
        try(Session s = HibernateUtil.getSession()){
            Query<MusicProduct> itemQuery = s.createQuery("from MusicProduct where price < :max", MusicProduct.class);
//            s.createNamedQuery("myQuery");
            itemQuery.setParameter("max",max);
            return itemQuery.list();
        }
    }

    @Override
    public List<MusicProduct> getItemsWithMinPrice(double min) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<MusicProduct> cq = cb.createQuery(MusicProduct.class);

            Root<MusicProduct> root = cq.from(MusicProduct.class);
            cq.select(root);

            cq.where(cb.greaterThan(root.get("price"), min));

            Query<MusicProduct> query = s.createQuery(cq);
            return query.list();
        }
    }

    @Override
    public MusicProduct addNewProduct(MusicProduct item) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            s.persist(item);
            int prod_id = (int) s.save(item);
            item.setProdId(prod_id);
            logger.info("added new item with id:"+prod_id);
            tx.commit();
            return item;
        }
    }

    @Override
    public void deleteProduct(int id) {
        try(Session s = HibernateUtil.getSession()){
            MusicProduct musicProduct = new MusicProduct(id);
            logger.info("TEST300: Order id -->{}<-- which need to MusicProduct list", musicProduct.getProdId());

            Transaction tx1 = s.beginTransaction();
            s.delete( musicProduct);
            tx1.commit();
        }
    }

    @Override
    public MusicProduct updateProduct(MusicProduct oldItem, MusicProduct newItem) {
        int prod_id = oldItem.getProdId();

        try (Session s = HibernateUtil.getSession()) {
            MusicProduct item = s.get(MusicProduct.class, prod_id);
            logger.info("old product id {} which need to replace: { }", prod_id, item);

            newItem.setProdId(prod_id);

            Transaction tx = s.beginTransaction();
            logger.info("110 new item prod_id {}  which  to replace with: {}", prod_id,  newItem.toString());

            s.merge(newItem);
            logger.info("update new user with same id:" + prod_id);
            logger.info("update new user info:" + newItem);

            tx.commit();
            return newItem;
        }
    }
}
