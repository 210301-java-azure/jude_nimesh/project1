package dev.nimesh.data;

import dev.nimesh.models.CustomerList;
import java.util.List;

public interface CustomerListDao {

    public List<CustomerList> getAllCustomers();
    public CustomerList getCustomerById(int id);
    public CustomerList addNewCustomer(CustomerList item);
    void deleteCustomer(int id);

    public CustomerList updateCustomer(CustomerList oldItem, CustomerList newItem);

}