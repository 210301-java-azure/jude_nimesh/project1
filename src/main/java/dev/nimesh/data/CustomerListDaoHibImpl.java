package dev.nimesh.data;

import dev.nimesh.models.CustomerList;
import dev.nimesh.models.UserList;
import dev.nimesh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class CustomerListDaoHibImpl implements CustomerListDao{

    private final Logger logger = LoggerFactory.getLogger(CustomerListDaoHibImpl.class);

    @Override
    public List<CustomerList> getAllCustomers() {
        try(Session s = HibernateUtil.getSession()){
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from customer_list'
            List<CustomerList> customers = s.createQuery("from CustomerList", CustomerList.class).list();
            return customers;
        }
    }

    @Override
    public CustomerList getCustomerById(int cust_id) {
        try(Session s = HibernateUtil.getSession()){
//          CustomerList customer = s.load(CustomerList.class, cust_id);
            CustomerList customer = s.get(CustomerList.class, cust_id);
//          logger.info("got customer from the database" + customer);
            return customer;
        }
    }

    @Override
    public CustomerList addNewCustomer(CustomerList customer) {
        //try (Session s = HibernateUtil.getSession()) {

            logger.info("Customer" + customer);
            boolean customerNotExist = true;

//----------------------------------------------------------------------passwordSignup_confirm--------
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//          s.persist(item);
            int id = (int) s.save(customer);
            customer.setCustomerId(id);
            logger.info("added new customer with id:"+id);
            tx.commit();
            return customer;
        }
    }


    @Override
    public void deleteCustomer(int id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(new CustomerList(id));
            tx.commit();
        }
    }

    @Override
    public CustomerList updateCustomer(CustomerList oldCustomer, CustomerList newCustomer) {
        int cust_id = oldCustomer.getCustomerId();

        try (Session s = HibernateUtil.getSession()) {
            CustomerList customer = s.get(CustomerList.class, cust_id);
            logger.info("old product id {} which need to replace: { }", cust_id, customer.toString());

            Transaction tx = s.beginTransaction();

            newCustomer.setCustomerId(cust_id);

            s.merge(newCustomer);
            logger.info("update new customer with same id:" + cust_id);
            logger.info("update new customer info:" + newCustomer.toString());
            logger.info("TEST: 1200 userId: "+newCustomer.getUser().getUserId());

            tx.commit();
            return newCustomer;
        }
    }
}
