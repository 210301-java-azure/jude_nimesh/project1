package dev.nimesh.data;

import dev.nimesh.models.OrderList;

import java.util.List;

public interface OrderListDao {

    public List<OrderList> getAllOrders();
    public OrderList getOrderById(int id);
    public OrderList addNewOrder(OrderList item);
    void deleteOrder(int id);

    public OrderList updateOrder(OrderList oldItem, OrderList newItem);

}