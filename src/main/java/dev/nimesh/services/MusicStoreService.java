package dev.nimesh.services;

import dev.nimesh.data.MusicProductDao;
import dev.nimesh.data.MusicProductDaoHibImpl;
import dev.nimesh.models.MusicProduct;
import io.javalin.http.BadRequestResponse;

import java.util.List;


public class MusicStoreService {

    //private MusicProductDao musicProductDao = new MusicProductDaoImpl();
    private final MusicProductDao musicProductDao = new MusicProductDaoHibImpl();

    public List<MusicProduct> getAll() {
        return musicProductDao.getAllProducts();
    }

    public MusicProduct getById(int id) {
        return musicProductDao.getProductById(id);
    }

    public MusicProduct add(MusicProduct item) {
        return musicProductDao.addNewProduct(item);
    }

    public void delete(int id) {
        musicProductDao.deleteProduct(id);
    }

    public void update(MusicProduct oldItem, MusicProduct newItem) {
        musicProductDao.updateProduct(oldItem, newItem);
    }

    public List<MusicProduct>  getPriceInRange(String minPriceStr, String maxPriceStr){
        double maxPrice;
        double minPrice;

        if( maxPriceStr != null) {
            // max provided
            // parse string maxPriceStr input to double
            maxPrice = Double.parseDouble(maxPriceStr);
            if (minPriceStr != null) {
                //if both are provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
//              return musicProductDao.getItemsInRange(minPrice, maxPrice); //(double min, double max);
            }
            //  only maxPrice provided
            // return musicProductDao.getItemWithMaxPrice( maxPrice ); // (double price );
        }else {
            if (minPriceStr != null) {
                // only min provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
                // return musicProductDao.getItemWithMinPrice( minPrice);  // (double price );
            }
        }
        return null;
    }

    public List<MusicProduct> getItemsInRange(String minPrice, String maxPrice) {
        if (minPrice != null) {
            if (matchesPriceFormat(minPrice)) {
                if (maxPrice != null) {
                    if (matchesPriceFormat(maxPrice)) {
                        return musicProductDao.getItemsInRange(Double.parseDouble(minPrice), Double.parseDouble(maxPrice));
                    }
                    throw new BadRequestResponse("improper price format provided for max-price");
                }
                // just min price
                return musicProductDao.getItemsWithMinPrice(Double.parseDouble(minPrice));
            }
            throw new BadRequestResponse("improper price format provided for min-price");
        } else {
            // just max price
            if (matchesPriceFormat(maxPrice)) {
                return musicProductDao.getItemsWithMaxPrice(Double.parseDouble(maxPrice));
            }
            throw new BadRequestResponse("improper price format provided for max-price");
        }
    }

    private boolean matchesPriceFormat(String str){
        return str.matches("\\d{0,4}(\\.\\d{1,2})?");
    }
}
