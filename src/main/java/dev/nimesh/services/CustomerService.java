package dev.nimesh.services;

import dev.nimesh.data.CustomerListDao;
import dev.nimesh.data.CustomerListDaoHibImpl;
import dev.nimesh.models.CustomerList;

import java.util.List;

public class CustomerService {

    //private CustomerListDao customerListDao = new CustomerListDaoImpl();
    private final CustomerListDao customerListDao = new CustomerListDaoHibImpl();

    public List<CustomerList> getAll() {
        return customerListDao.getAllCustomers();
    }

    public CustomerList getCustomerListById(int user_id) {
//System.out.println("Customer ID: "+user_id+"\n"+ customerListDao.getCustomerById(user_id).toString() );
        return customerListDao.getCustomerById(user_id);
    }

    public CustomerList add(CustomerList user) {
        return customerListDao.addNewCustomer(user);
    }

    public void delete(int user_id) {
        customerListDao.deleteCustomer(user_id);
    }

    public void update(CustomerList oldUser, CustomerList newUser) {
        customerListDao.updateCustomer(oldUser, newUser);
    }


    public List<CustomerList>  getPriceInRange(String minPriceStr, String maxPriceStr){
        double maxPrice;
        double minPrice;

        if( maxPriceStr != null) {
            // max provided
            // parse string maxPriceStr input to double
            maxPrice = Double.parseDouble(maxPriceStr);
            if (minPriceStr != null) {
                //if both are provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
//                return customerListDao.getItemsInRange(minPrice, maxPrice); //(double min, double max);
            }
            //  only maxPrice provided
            // return customerListDao.getItemWithMaxPrice( maxPrice ); // (double price );
        }else {
            if (minPriceStr != null) {
                // only min provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
                //   return customerListDao.getItemWithMinPrice( minPrice);  // (double price );
            }
        }
        return null;
    }
}
