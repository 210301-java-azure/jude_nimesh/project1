package dev.nimesh.services;

import dev.nimesh.data.*;
import dev.nimesh.models.UserList;

import java.util.List;

public class UserService {

    //private UserListDao userListDao = new UserListDaoImpl();
    private  UserListDao userListDao = new UserListDaoHibImpl();


    public List<UserList> getAll() {
            return userListDao.getAllUsers();
    }

    public UserList getUserListById(int id) {
        System.out.println("500: User id: "+id+"\n"+ userListDao.getUserById(id).toString() );
        return userListDao.getUserById(id);
    }

    public UserList getUserNamePassword(String username) {
        System.out.println("600: username : "+username+" \n"+
                userListDao.getUserByEmail(username).toString() );
        return userListDao.getUserByEmail(username);
    }


    public UserList add(UserList user) {
        return userListDao.addNewUser(user);
    }

    public void delete(int id) {
        userListDao.deleteUser(id);
    }

    public void update(UserList oldUser, UserList newUser) {
        userListDao.updateNewUser(oldUser, newUser);
    }

}

