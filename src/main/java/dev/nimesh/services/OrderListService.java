package dev.nimesh.services;

import dev.nimesh.data.*;
import dev.nimesh.models.OrderList;
import java.util.List;

public class OrderListService {

    //private OrderListDao orderListDao = new OrderListDaoImpl();
    private final OrderListDao orderListDao = new OrderListDaoHibImpl();


    public List<OrderList> getAll() {
        return orderListDao.getAllOrders();
    }

    public OrderList getById(int order_id) {

        return orderListDao.getOrderById(order_id);
    }

    public OrderList add(OrderList order) {

        return orderListDao.addNewOrder(order);
    }

    public void delete(int order_id) {

        orderListDao.deleteOrder(order_id);
    }

    public void update(OrderList oldOrder, OrderList newOrder) {

        orderListDao.updateOrder(oldOrder, newOrder);
    }


    public List<OrderList>  getPriceInRange(String minPriceStr, String maxPriceStr){
        double maxPrice;
        double minPrice;

        if( maxPriceStr != null) {
            // max provided
            // parse string maxPriceStr input to double
            maxPrice = Double.parseDouble(maxPriceStr);
            if (minPriceStr != null) {
                //if both are provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
//                return orderListDao.getItemsInRange(minPrice, maxPrice); //(double min, double max);
            }
            //  only maxPrice provided
            // return orderListDao.getItemWithMaxPrice( maxPrice ); // (double price );
        }else {
            if (minPriceStr != null) {
                // only min provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
                //   return orderListDao.getItemWithMinPrice( minPrice);  // (double price );
            }
        }
        return null;
    }
}
