package dev.nimesh.models;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dev.nimesh.data.MusicProductDao;
import dev.nimesh.models.MusicProduct;
import dev.nimesh.models.CustomerList;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//import javax.persistence.GenerationType;

@Entity
@Table(name="OrderList")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class OrderList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int order_id;

    private String order_name;

    private int qty;

    private double total;

    // @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne
    @Cascade({CascadeType.ALL})
    private CustomerList customer;      //---------from class CustomerList ------

    @ManyToOne
    @Cascade({CascadeType.ALL})
    private MusicProduct musicProduct;  //---------from class MusicProduct ------


    // define all constructor methods
    public OrderList(){
        super();
    }

    public OrderList(int order_id, String order_name, int qty, double total) {
        super();
        this.order_id   = order_id;
        this.order_name = order_name;
        this.qty        = qty;
        this.total      = total;
    }

    public OrderList(int order_id, String order_name, int qty, double total,
                     CustomerList customer, MusicProduct musicProduct) {
        super();
        this.order_id     = order_id;
        this.order_name   = order_name;
        this.qty          = qty;
        this.total        = total;
        this.customer     = customer;
        this.musicProduct = musicProduct;
    }

    public OrderList(String order_name, int qty, double total, CustomerList customer, MusicProduct musicProduct) {
        super();
        this.order_name   = order_name;
        this.qty          = qty;
        this.total        = total;
        this.customer     = customer;
        this.musicProduct = musicProduct;
    }

    public OrderList(String order_name, int qty, double total) {
        super();
        this.order_name   = order_name;
        this.qty          = qty;
        this.total        = total;
    }

    public OrderList(int order_id) {
        this.order_id = order_id;
    }
    public OrderList(String order_name) {
        this.order_name = order_name;
    }

    //public OrderList(MusicProduct musicProduct) {
    //    this.prod_name = musicProduct.getProdName();
    //    this.price = musicProduct.getPrice();
    //}

    //public OrderList(CustomerList customer) {
    //    this.cust_name = customer.getCustomerName();
    //}

    public int getOrderId() { return order_id; }

    public void setOrderId(int order_id) { this.order_id = order_id; }

    public String getOrderName() {
        return order_name;
    }

    public void setOrderName(String order_name) {
        this.order_name = order_name;
    }

    public double getOrderTotal() { return total; }

    public void setOrderTotal(double total) { this.total = total; }

    public int getOrderQuantity() { return qty; }

    public void setOrderQuantity(int qty) { this.qty = qty; }

    public CustomerList getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerList customer) {
        this.customer = customer;
    }

    public MusicProduct getMusicProduct() {
        return musicProduct;
    }

    public void setMusicProduct(MusicProduct musicProduct) {
        this.musicProduct = musicProduct;
    }

}

