package dev.nimesh.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="CustomerList")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class CustomerList  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cust_id;

    private String cust_name;

    private String address;

    private String phone;

    // @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne
    @Cascade({CascadeType.ALL})
    private UserList user;

    //constructor methods
    public CustomerList(){
        super();
    }

    public CustomerList( UserList user) { this.user = user; }

    public CustomerList(int cust_id) {
        this.cust_id = cust_id;
    }

    public CustomerList(int cust_id, String cust_name, String address, String phone, UserList user) {
        this.cust_id   = cust_id;
        this.cust_name = cust_name;
        this.address   = address;
        this.phone     = phone;
        this.user      = user;
    }

    public CustomerList(String cust_name, String address, String email, String phone, UserList user) {
        this.cust_name = cust_name;
        this.address   = address;
        this.phone     = phone;
        this.user      = user;
    }


    // setters and getters
    public int getCustomerId() {
        return cust_id;
    }

    public void setCustomerId(int cust_id) {
        this.cust_id = cust_id;
    }

    public String getCustomerName() {
        return cust_name;
    }

    public void setCustomerName(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserList getUser() {
        return user;
    }

    public void setUser(UserList user) {
        this.user = user;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    //public String getPassword() { return password; }
    //public void setPassword(String password) { this.password = password; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerList that = (CustomerList) o;
        return cust_id == that.cust_id && Objects.equals(cust_name, that.cust_name) &&
                Objects.equals(address, that.address) && Objects.equals(user, that.user) &&
                Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cust_id, cust_name, address, phone, user);
    }

    @Override
    public String toString() {
        return "CustomerList{" +
               "cust_id=" + cust_id +
               ", cust_name='" + cust_name + '\'' +
               ", address='" + address + '\'' +
               ", phone='" + phone + '\'' +
               ", user='" + user + '\'' +
               '}';
    }
}

