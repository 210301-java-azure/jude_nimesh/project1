package dev.nimesh.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String state;
    private Date creationDate;

    //@OneToOne
    //private Employee manager;


    public Department(int id, String name, String state) {
        this.id    = id;
        this.name  = name;
        this.state = state;
    }

    public Department() {
        super();
    }

    public Department(int id) {
        this.id = id;
    }

    public Department(String name, String state) {
        this.name  = name;
        this.state = state;
    }

    public Department(String name, String state, Date creationDate) {
        this.name  = name;
        this.state = state;
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return id == that.id && name.equals(that.name) && state.equals(that.state) &&
                creationDate.equals(that.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, state, creationDate);
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
