package dev.nimesh.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.bytebuddy.implementation.bind.MethodDelegationBinder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="UserList")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class UserList  implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int  id;

    @Column(unique=true)
    private String email;

    private String password;

    public UserList(){
        super();
    }

    public UserList(int id) {
        this.id = id;
    }

    public UserList(String email) {
        this.email = email;
    }

    public UserList(String email, String password) {
        this.email    = email;
        this.password = password;
    }

    public UserList(int  id, String email, String password) {
        this.id       = id;
        this.email    = email;
        this.password = password;
    }

    // getters and setters ...
    public int getUserId() {
        return id;
    }

    public void setUserId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserList userList = (UserList) o;
        return Objects.equals(id, userList.id) && Objects.equals(email, userList.email)
                && Objects.equals(password, userList.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password);
    }

    @Override
    public String toString() {
        return "UserList{" +
                "user id='" + id + '\'' +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}