package dev.nimesh.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

//@NamedQueries(@NamedQuery(name = "myQuery", query = "from ..."))

@Entity
@Table(name="MusicProduct")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class MusicProduct implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int prod_id;

    private String prod_name;
    
    public String section;
    
    private double price;
    
    private int stock;

    // @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne
    private Department department;

    // constructor methods
    public MusicProduct(){
        super();
    }

    public MusicProduct(int prod_id) {
        this.prod_id = prod_id;
    }

    public MusicProduct(String prod_name, double price, int stock ) {
        this.prod_name = prod_name;
        this.price     = price;
        this.stock     = stock;
    }

    public MusicProduct(String prod_name, String section, double price, int stock ) {
        this.prod_name  = prod_name;
        this.price      = price;
        this.stock      = stock;
        this.section    = section;
    }

    public MusicProduct(int prod_id, String prod_name, String section, double price, int stock ) {
        this.prod_id    = prod_id;
        this.prod_name  = prod_name;
        this.price      = price;
        this.stock      = stock;
        this.section    = section;
    }

    public MusicProduct(int prod_id, String prod_name, String section, double price, int stock, Department dept ) {
        this.prod_id    = prod_id;
        this.prod_name  = prod_name;
        this.price      = price;
        this.stock      = stock;
        this.section    = section;
        this.department = dept;
    }
    // getts and setters
    public int getProdId() { return prod_id; }

    public void setProdId(int id) { this.prod_id = prod_id; }

    public String getProdName() { return prod_name; }

    public void setProdName(String name) { this.prod_name = name; }

    public String getSection() { return section; }

    public void setSection(String section) { this.section = section; }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) { this.stock = stock; }

    public double getPrice() {
        return price;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public void setPrice(double price) { this.price = price; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MusicProduct product = (MusicProduct) o;
        return prod_id == product.prod_id && Double.compare(product.price, price) == 0 && stock == product.stock
                && Objects.equals(prod_name, product.prod_name) && Objects.equals(section, product.section)
                && Objects.equals(department, product.department);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prod_id, prod_name, section, price, stock, department);
    }

    @Override
    public String toString() {
        return "MusicProduct{" +
                "prod_id=" + prod_id +
                ", prod_name='" + prod_name + '\'' +
                ", section='" + section + '\'' +
                ", price=" + price + '\''+
                ", stock=" + stock + "\n"+'\'' +
                ", department=" + department + "\n"+'\'' +
                '}';
    }
}
