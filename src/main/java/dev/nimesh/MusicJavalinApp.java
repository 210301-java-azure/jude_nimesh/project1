package dev.nimesh;

import dev.nimesh.controllers.*;
import dev.nimesh.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.post;

public class MusicJavalinApp {

    ProductController productController     = new ProductController();
    OrderListController orderListController = new OrderListController();
    CustomerController customerController   = new CustomerController();
    
    AuthController authController = new AuthController();
    UserController userController = new UserController();
    SecurityUtil securityUtil     = new SecurityUtil();

    // adding
    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(()->{

        path("/items", ()-> {
            before("/", authController::authorizeToken);
            get(productController::handleGetProductRequest);
            post(productController::handlePostNewProduct);

            path("/:prodId", () -> {
                before("/", authController::authorizeToken);
                get(productController::handleGetProductByIdRequest);
                delete(productController::handleDeleteProductById);
                put(productController::handleUpdateProductById);
            });
        });

        //before("/users", authController::authorizeToken);
        path("/users", ()-> {
            before("/", authController::authorizeToken);
            get(userController::handleGetUserRequest);
            post(userController::handlePostNewUser);
            path("/:userId", () -> {
                before("/", authController::authorizeToken);
                get(userController::handleGetUserByIdRequest);
                delete(userController::handleDeleteUserById);
                put(userController::handleUpdateUserById);
            });
        });

        path("/customers", ()-> {
            before("/", authController::authorizeToken);
            get(customerController::handleGetCustomerRequest);
            post(customerController::handlePostNewCustomer);
            path("/:customerId", () -> {
                before("/", authController::authorizeToken);
                get(customerController::handleGetCustomerByIdRequest);
                delete(customerController::handleDeleteCustomerById);
                put(customerController::handleUpdateCustomerById);
            });
        });

        path("/orders", ()->{
            before("/", authController::authorizeToken);
            get(orderListController::handleGetOrderRequest);
            post(orderListController::handlePostNewOrder);
            path("/:orderId",()->{
                before("/", authController::authorizeToken);
                get(orderListController::handleGetOrderByIdRequest);
                delete(orderListController::handleDeleteOrderById);
                put(orderListController::handleUpdateOrderById);
            });
        });

        path("login",()-> {
            post(authController::authenticateLogin);
            after("/", securityUtil::attachResponseHeaders);
        });
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
