package dev.nimesh.util;

import dev.nimesh.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            //settings.put(Environment.URL, "jdbc:sqlserver://crehm-java-azure.database.windows.net:1433;databaseName=training-db");
            //DB_URL=jdbc:sqlserver://nimesh-server.database.windows.net:1433;databaseName=nimesh-java-azure-training
            settings.put(Environment.URL, System.getenv("DB_URL"));
            //DB_USER=jnimesh@nimesh-server
            settings.put(Environment.USER, System.getenv("DB_USER"));
            //DB_PASS=!Jesus323"
            settings.put(Environment.PASS, System.getenv("DB_PASS"));

            //https://docs.microsoft.com/en-us/sql/connect/jdbc/working-with-a-connection?view=sql-server-ver15
            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //https://www.javatpoint.com/dialects-in-hibernate
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "update"); //create here is to create tables // once created replace with validate
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //provide hibernate mappings to configuration
            configuration.addAnnotatedClass(MusicProduct.class);
            configuration.addAnnotatedClass(Department.class);
            configuration.addAnnotatedClass(UserList.class);
            configuration.addAnnotatedClass(OrderList.class);
            configuration.addAnnotatedClass(CustomerList.class);




            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }

}
