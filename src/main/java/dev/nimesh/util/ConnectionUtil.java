package dev.nimesh.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static Connection connection;
    private ConnectionUtil(){
        super();
    }
    public static Connection getConnection() throws SQLException {
        if(connection == null || connection.isClosed()) {
            // this connection can be found on Azure
            String connectionUrl = System.getenv("connectionUrl");
            connection = DriverManager.getConnection(connectionUrl);
        }
        return connection;
    }
}