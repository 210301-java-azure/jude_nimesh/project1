package dev.nimesh;

public class MusicProductDriver {

    public static void main(String[] args) {

        MusicJavalinApp  musicJavalinApp = new MusicJavalinApp();
        musicJavalinApp.start(80);
        System.out.println("ENV for URL: "+ System.getenv("DB_URL"));
        System.out.println("ENV for DB_USER: "+ System.getenv("DB_USER"));
        System.out.println("ENV for DB_PASS: "+ System.getenv("DB_PASS"));

    }

}