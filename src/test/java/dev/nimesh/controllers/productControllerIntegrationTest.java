package dev.nimesh.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.nimesh.MusicJavalinApp;
import dev.nimesh.models.MusicProduct;

import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class productControllerIntegrationTest {

    private static MusicJavalinApp app = new MusicJavalinApp();

    @BeforeAll
    public static void startService(){
        app.start(7000);
    }

    @AfterAll
    public static void stopService(){
        app.stop();
    }

    @Test
    public void testGetAllItemsUnauthorized(){
        HttpResponse<String> response = Unirest.get("http://localhost:7000/items").asString();
        assertAll(
                ()->assertEquals( 401,response.getStatus()),
                ()->assertEquals( "Unauthorized",response.getBody()));
    }

    @Test
    public void testGetAllItemsAuthorized(){
        HttpResponse<List<MusicProduct>> response = Unirest.get("http://localhost:7000/items")
                .header("Authorization", "admin-auth-token")
                .asObject(new GenericType<List<MusicProduct>>() {});
        assertAll(
                ()->assertEquals(200,response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }

    @Test
    public void testGetOneAuthorized(){
        HttpResponse<MusicProduct> response = Unirest.get("http://localhost:7000/items/1")
                .header("Authorization", "admin-auth-token")
                .asObject(MusicProduct.class);
        // assertAll is better than just including 3 assert methods by themselves
        // the other assert methods will not execute otherwise, if one of the earlier assertions fails
        assertAll(
                ()->assertNotNull(response.getBody()),
                ()->assertEquals(1,response.getBody().getProdId()),
                ()->assertEquals(200, response.getStatus())
        );
    }

    @Test
    public void testGetOneNotInDbAuthorized(){
        int testId = 100;
        HttpResponse<String> response = Unirest.get("http://localhost:7000/items/"+100)
                .header("Authorization", "admin-auth-token")
                .asString();
        // assertAll is better than just including 3 assert methods by themselves
        // the other assert methods will not execute otherwise, if one of the earlier assertions fails
        assertAll(
                ()->assertEquals("No item found with provided ID: "+100,response.getBody()),
                ()->assertEquals(404, response.getStatus())
        );
    }

    @Test
    public void testAddNewItemAuthorized() throws JsonProcessingException {
        MusicProduct newItem = new MusicProduct("piano", "instruments", 1750.99, 5);  
        HttpResponse<MusicProduct> response = Unirest.post("http://localhost:7000/items")
                // object mapper comes from Jackson dependency
                .body(new ObjectMapper().writeValueAsString(newItem))
                .header("Authorization", "admin-auth-token")
                .asObject(MusicProduct.class);
        assertAll(
                ()->assertNotEquals(0, response.getBody().getProdId()),
                ()->assertEquals(201, response.getStatus())
        );
    }
}
