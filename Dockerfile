FROM java:8
COPY build/libs/musicstore_server-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar musicstore_server-1.0-SNAPSHOT.jar
